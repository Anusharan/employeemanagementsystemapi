﻿using AutoMapper;
using EMS.Data.Models;
using EMSystemAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMSystemAPI.AutoMapperProfile
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            #region Contact

            CreateMap<Contact, ContactViewModel>();
            CreateMap<ContactViewModel, Contact>();

            #endregion

            #region Employee

            CreateMap<Employee, EmployeeViewModel>();
            CreateMap<EmployeeViewModel, Employee>();

            #endregion

            #region Department

            CreateMap<Department, DepartmentViewModel>();
            CreateMap<DepartmentViewModel, Department>();

            #endregion

        }
    }
}
