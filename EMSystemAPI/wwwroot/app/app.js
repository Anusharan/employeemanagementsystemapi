﻿(function () {
    'use strict';
    angular.module('myapp', ['ui.router', 'ui.bootstrap', 'file-model'])

        .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/dashboard');
            $stateProvider
                .state("contact", {
                    url: '/contact',
                    templateUrl: '../views/contact/contactpage.html',
                    controller: 'contactCtrl'
                })
                .state("employee", {
                    url: '/employee',
                    templateUrl: "../views/employee/employeepage.html",
                    controller: 'employeeCtrl'
                })
                .state("department", {
                    url: '/department',
                    templateUrl: "../views/department/departmentepage.html",
                    controller: 'departmentCtrl'
                })
                .state("dashboard", {
                    url: '/dashboard',
                    templateUrl: '../views/dashboard/dashboardpage.html',
                    controller: 'employeeCtrl'
                })

        }]);
})();


