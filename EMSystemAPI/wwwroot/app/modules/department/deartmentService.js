﻿(function () {
    angular.module('myapp')
        .service('departmentService', departmentService);

    departmentService.$inject = ['$http'];

    function departmentService($http) {
        var urlGet = '';
        this.post = function (apiRoute, Model) {
            var request = $http({
                method: "post",
                url: apiRoute,
                data: Model
            });
            return request;
        }

        this.put = function (apiRoute, Model) {
            var request = $http({
                method: "put",
                url: apiRoute,
                data: Model
            })
            return request;
        }

        this.delete = function (apiRoute) {
            var request = $http({
                method: "delete",
                url: apiRoute
            })
            return request;
        }

        this.getAll = function (apiRoute) {
            urlGet = apiRoute;
            return $http.get(urlGet);
        }

        this.get = function (apiRoute, departmentId) {
            urlGet = apiRoute + '/' + departmentId;
            return $http.get(urlGet);
        }
    }

})();