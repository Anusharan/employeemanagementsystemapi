﻿(function () {
    'use strict';

    angular.module('myapp')
        .controller('departmentCtrl', departmentCtrl);

    departmentCtrl.$inject = ['departmentService'];

    function departmentCtrl(departmentService) {
        var vm = this;
        var baseUrl = '/api/Department/';
        vm.btnText = "Save";
        vm.departmentId = 0;

        //pagination
        vm.maxSize = 5;
        vm.totalCount = 0;
        vm.pageIndex = 1;
        vm.pageSize = 5;

        vm.SaveUpdate = function () {
            var department = {
                DepartmentName: vm.departmentName,
                DepartmentDescription: vm.departmentDescription,
                DepartmentId: vm.departmentId
            }
            if (vm.btnText == "Save") {
                var apiRoute = baseUrl;
                var saveDepartment = departmentService.post(apiRoute, department);
                saveDepartment.then(function (response) {
                    if (response.data != "") {
                        alert("Data Saved Successfully");
                        GetDepartments();
                        closePopUp();                    }
                    else {
                        alert("Error Saving Data");
                    }
                },
                    function (error) {
                        console.log("Error: " + error);
                    });
            } else {
                var apiRoute = baseUrl + department.DepartmentId;
                var UpdateDepartment = departmentService.put(apiRoute, department);
                UpdateDepartment.then(function (response) {
                    if (response.data != "") {
                        alert("Data Update Successfully");
                        GetDepartments();
                        closePopUp();
                    } else {
                        alert("Some error");
                    }
                }, function (error) {
                    console.log("Error: " + error);
                });
            }
        }

        function GetDepartments() {
            var apiRoute = baseUrl;
            var begin = ((vm.pageIndex - 1) * vm.pageSize), end = begin + vm.pageSize;
            var departments = departmentService.getAll(apiRoute);
            departments.then(function (response) {
                vm.departments = response.data;
                vm.paginateddepartments = vm.departments.slice(begin, end);
            }, function (error) {
                console.log("Error: " + error);
            });
        }
        GetDepartments();

        vm.GetDepartmentById = function (dataModel) {
            var apiRoute = baseUrl;
            var department = departmentService.get(apiRoute, dataModel.departmentId);
            department.then(function (response) {
                vm.departmentId = response.data.departmentId;
                vm.departmentName = response.data.departmentName;
                vm.departmentDescription = response.data.departmentDescription;
                vm.btnText = "Update";
            }, function (error) {
                console.log("Error: " + error);
            });
        }

        vm.DeleteDepartment = function (dataModel) {
            var apiRoute = baseUrl + dataModel.departmentId;
            var deleteDepartment = departmentService.delete(apiRoute);
            deleteDepartment.then(function (response) {
                if (response.data != "") {
                    alert("Data Deleted Successfully");
                    GetDepartments();
                    //vm.Clear();
                }
                else {
                    alert("Error Deleting Data");
                }
            },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        function closePopUp() {
            $('#exampleModal').modal('hide');
        }
        closePopUp();

        vm.departmentPageChanged = function () {
            GetDepartments();
        }
    }
})();