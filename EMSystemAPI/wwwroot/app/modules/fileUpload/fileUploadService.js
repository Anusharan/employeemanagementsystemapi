﻿(function () {
    angular.module('myapp')
        .service('fileUploadService', fileUploadService);

    fileUploadService.$inject = ['$http'];

    function fileUploadService($http) {
        this.post = function (file, uploadUrl) {
            var fd = new FormData();
            fd.append('file', file);
            var request = $http({
                method: "post",
                url: uploadUrl,
                data: fd,
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }

            });
            return request;
        }      
    }

})();