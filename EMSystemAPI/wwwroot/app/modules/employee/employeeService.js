﻿(function () {
    angular.module('myapp')
        .service('employeeService', employeeService);

    employeeService.$inject = ['$http'];

    function employeeService($http) {
        var urlGet = '';
        this.post = function (apiRoute, Model) {
            var request = $http({
                method: "post",
                url: apiRoute,
                data: Model
            });
            return request;
        }

        this.put = function (apiRoute, Model) {
            var request = $http({
                method: "put",
                url: apiRoute,
                data: Model
            })
            return request;
        }

        this.delete = function (apiRoute) {
            var request = $http({
                method: "delete",
                url: apiRoute
            })
            return request;
        }

        this.getAll = function (apiRoute) {
            urlGet = apiRoute;
            return $http.get(urlGet);
        }

        this.get = function (apiRoute, employeeId) {
            urlGet = apiRoute + '/' + employeeId;
            return $http.get(urlGet);
        }
    }

})();