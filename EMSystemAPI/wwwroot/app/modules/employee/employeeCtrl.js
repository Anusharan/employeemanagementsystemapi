﻿(function () {
    'use strict';

    angular.module('myapp')
        .controller('employeeCtrl', employeeCtrl);

    employeeCtrl.$inject = ['$scope','employeeService', 'departmentService', 'contactService'];

    function employeeCtrl($scope,employeeService, departmentService, contactService) {
        var vm = this;
        var baseUrl = '/api/Employee/';
        var paginationUrl = '/api/Employee/GetDepartmentEmployeesPagination'
        var departmentBaseUrl = '/api/Department/';
        var contactBaseUrl = '/api/Contact/';
        vm.btnText = "Save";
        vm.employeeID = 0;
        vm.departmentList = [];

        //pagination
        vm.maxSize = 5;
        vm.totalCount = 0;
        vm.pageIndex = 1;
        vm.pageSize = 5;

        vm.SaveUpdate = function () {
            var employee = {
                Salary: vm.salary,
                IsAdim: vm.isAdim,
                IsActive: vm.isActive,
                StartDate: vm.startDate,
                DepartmentId: vm.departmentId,
                EmployeeId: vm.employeeId,
                ContactId: vm.contactId
            }
            if (vm.btnText == "Save") {
                var apiRoute = baseUrl;
                var saveEmployee = employeeService.post(apiRoute, employee);
                saveEmployee.then(function (response) {
                    if (response.data != "") {
                        alert("Data Saved Successfully");
                        GetEmployees();
                        closePopUp();
                    }
                    else {
                        alert("Error Saving Data");
                    }
                },
                    function (error) {
                        console.log("Error: " + error);
                    });
            } else {
                var apiRoute = baseUrl + employee.EmployeeId;
                var UpdateEmployee = employeeService.put(apiRoute, employee);
                UpdateEmployee.then(function (response) {
                    if (response.data != "") {
                        alert("Data Update Successfully");
                        GetEmployees();
                        closePopUp();
                    } else {
                        alert("Some error");
                    }
                }, function (error) {
                    console.log("Error: " + error);
                });
            }
        }

        function GetEmployees() {
            var apiRoute = baseUrl;
            var begin = ((vm.pageIndex - 1) * vm.pageSize), end = begin + vm.pageSize;
            var employees = employeeService.getAll(apiRoute);
            employees.then(function (response) {
                vm.employees = response.data;
                vm.paginatedemployees = vm.employees.slice(begin, end);
            }, function (error) {
                console.log("Error: " + error);
            });
        }
        GetEmployees();

        vm.GetEmployeeById = function (dataModel) {
            $('#exampleModal').modal('show');
            var apiRoute = baseUrl;
            var employee = employeeService.get(apiRoute, dataModel.employeeId);
            employee.then(function (response) {
                vm.employeeId = response.data.employeeId;
                vm.contactId = response.data.contactId;
                vm.departmentId = response.data.departmentId;
                vm.salary = response.data.salary;
                vm.isAdim = response.data.isAdim;
                vm.isActive = response.data.isActive;
                vm.startDate = response.data.startDate;
                vm.btnText = "Update";
            }, function (error) {
                console.log("Error: " + error);
            });
        }

        vm.DeleteEmployee = function (dataModel) {
            var apiRoute = baseUrl + dataModel.employeeId;
            var deleteEmployee = employeeService.delete(apiRoute);
            deleteEmployee.then(function (response) {
                if (response.data != "") {
                    alert("Data Deleted Successfully");
                    GetEmployees();
                    //vm.Clear();
                }
                else {
                    alert("Error Deleting Data");
                }
            },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        function GetDepartmentDetails() {
            departmentService.getAll(departmentBaseUrl)
            .then(function (data) {
                vm.departmentList = data.data;
            });
        }
        GetDepartmentDetails();

        function GetContactDetails() {
            contactService.getAll(contactBaseUrl)
                .then(function (data) {
                    vm.contactList = data.data;
                });
        }
        GetContactDetails();

        function closePopUp() {
            $('#exampleModal').modal('hide');
        }
        closePopUp();

        function GetDashboardEmployees() {
            dashboardlist();
        }
        GetDashboardEmployees();

        //dashboard pagination
        function dashboardlist() {
            var apiRoute = paginationUrl + "?pageSize=" + vm.pageSize + "&pageNumber=" + vm.pageIndex;
            console.log('pagination url', apiRoute);
            var dashboardEmployeesList = employeeService.getAll(apiRoute);
            dashboardEmployeesList.then(function (response) {
                console.log('response', response);
                vm.dashboardEmployees = response.data.departmentEmployeeList;
                vm.dashboardTotalCount = response.data.totalCount;
            }, function (error) {
                console.log("Error: " + error);
            });
        }
        dashboardlist();

        vm.changePageSize = function () {
            vm.pageIndex = 1;
            dashboardlist();
        }

        vm.pageChanged = function () {
            dashboardlist();
        }

        vm.employeePageChanged = function () {
            GetEmployees();
        }

        //vm.Clear = function() {
        //    debugger
        //        vm.salary = '',
        //        vm.isAdim = '',
        //        vm.isActive = '',
        //        vm.startDate = '',
        //        vm.departmentId = '',
        //        vm.employeeId ='',
        //        vm.contactId = '',
        //        vm.btnText = "Save";
        //}
    }
})();