﻿(function () {
    'use strict';

    angular.module('myapp')
        .controller('contactCtrl', contactCtrl);

    contactCtrl.$inject = ['contactService','fileUploadService'];

    function contactCtrl(contactService, fileUploadService) {
        var vm = this;
        var baseUrl = '/api/Contact/';
        vm.btnText = "Save";
        vm.titleText = "Add Content"
        vm.contactID = 0;
        vm.imagePath = '';

        //pagination
        vm.maxSize = 5;
        vm.totalCount = 0;
        vm.pageIndex = 1;
        vm.pageSize = 5;

        vm.SaveUpdate = function () {
            vm.uploadFile();
        }

        function GetContacts() {
            var apiRoute = baseUrl;
            var begin = ((vm.pageIndex - 1) * vm.pageSize), end = begin + vm.pageSize;
            var contacts = contactService.getAll(apiRoute);
            contacts.then(function (response) {
                vm.contacts = response.data;
                vm.paginatedContacts = vm.contacts.slice(begin, end);
            }, function (error) {
                console.log("Error: " + error);
            });
        }
        GetContacts();

        vm.GetContactById = function (dataModel) {
            $('#exampleModal').modal('show');
            var apiRoute = baseUrl;
            var contact = contactService.get(apiRoute, dataModel.contactId);
            contact.then(function (response) {
                vm.contactId = response.data.contactId;
                vm.firstName = response.data.firstName;
                vm.lastName = response.data.lastName;
                vm.phoneNumber = response.data.phoneNumber;
                vm.gender = response.data.gender;
                vm.email = response.data.email;
                vm.address = response.data.address;
                vm.country = response.data.country;
                vm.city = response.data.city;
                vm.myFile = response.data.image;
                vm.btnText = "Update";
                vm.titleText = "Update Content"
            }, function (error) {
                console.log("Error: " + error);
            });
        }

        vm.DeleteContact = function (dataModel) {
            var apiRoute = baseUrl + dataModel.contactId;
            var deleteContact = contactService.delete(apiRoute);
            deleteContact.then(function (response) {
                if (response.data != "") {
                    alert("Data Deleted Successfully");
                    GetContacts();
                }
                else {
                    alert("Error Deleting Data");
                }
            },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        function closePopUp() {
            $('#exampleModal').modal('hide');
        }
        closePopUp();

        vm.contactPageChanged = function () {
            GetContacts();
        }

        vm.uploadFile = function () {
            var file = vm.myFile;
            console.dir(file);
            var uploadUrl = "/api/fileUpload/";  // upload url stands for api endpoint to handle upload to directory
            var fileUpload = fileUploadService.post(file, uploadUrl);
            fileUpload.then(function (response) {
                if (response.data != "") {
                    alert("File Uploaded successfully!");
                    vm.imagePath = response.data.imagePath;
                    vm.saveContact();
                } else {
                    alert("Some error");
                }
            }, function (error) {
                console.log("Error: " + error);
            });
        }

        vm.saveContact = function () {
            var contact = {
                FirstName: vm.firstName,
                LastName: vm.lastName,
                PhoneNumber: vm.phoneNumber,
                Gender: vm.gender,
                Email: vm.email,
                Address: vm.address,
                Country: vm.country,
                City: vm.city,
                ContactId: vm.contactId,
                Image: vm.imagePath
            }
            if (vm.btnText == "Save") {
                var apiRoute = baseUrl;
                var saveContact = contactService.post(apiRoute, contact);
                saveContact.then(function (response) {
                    if (response.data != "") {
                        alert("Data Saved Successfully");
                        GetContacts();
                        closePopUp();
                    }
                    else {
                        alert("Error Saving Data");
                    }
                },
                    function (error) {
                        console.log("Error: " + error);
                    });
            } else {
                var apiRoute = baseUrl + contact.ContactId;
                var UpdateContact = contactService.put(apiRoute, contact);
                UpdateContact.then(function (response) {
                    if (response.data != "") {
                        alert("Data Update Successfully");
                        GetContacts();
                        closePopUp();
                    } else {
                        alert("Some error");
                    }
                }, function (error) {
                    console.log("Error: " + error);
                });
            }
        }

    }
})();