﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMSystemAPI.ViewModel
{
    public class ContactViewModel
    {
        public int ContactId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public DateTime? CreatedTs { get; set; }
        public DateTime? ModifiedTs { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public int? CompanyId { get; set; }
        public string Gender { get; set; }
        public string Image { get; set; }

        public ContactViewModel()
        {
            CreatedTs = DateTime.Now;
            ModifiedTs = DateTime.Now;
            CreatedBy = 1;
            CompanyId = 1;
        }
    }
}
