﻿using EMS.Data.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EMSystemAPI.ViewModel
{
    public class EmployeeViewModel
    {
        public int EmployeeId { get; set; }
        public int ContactId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? Salary { get; set; }
        public bool? IsAdim { get; set; }
        public DateTime? CreatedTs { get; set; }
        public DateTime? ModifiedTs { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public bool? IsActive { get; set; }
        public int? DepartmentId { get; set; }
        public int? CompanyId { get; set; }
        public string UserId { get; set; }

        public  ContactViewModel Contact { get; set; }
        public  DepartmentViewModel Department { get; set; }

        public EmployeeViewModel()
        {
            CreatedTs = DateTime.Now;
            ModifiedTs = DateTime.Now;
            CreatedBy = 1;
            CompanyId = 1;
        }
    }

    public class EmployeesListViewModel
    {
        public IEnumerable<DepartmentEmployee> DepartmentEmployeeList { get; set; }
        public int TotalCount { get; set; }
    }
}
