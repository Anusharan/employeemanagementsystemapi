﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EMS.Data.Models;
using EMS.Data.Models.ViewModel;
using EMS.Service.Employee;
using EMSystemAPI.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace EMSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : Controller
    {
        private readonly IEmployeeService _employeeService;
        private readonly IMapper _mapper;

        public EmployeeController(IEmployeeService employeeService, IMapper mapper)
        {
            _employeeService = employeeService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetEmployees()
        {
            IEnumerable<Employee> employeeModel = _employeeService.GetAll();
            IEnumerable<EmployeeViewModel> employees = _mapper.Map<List<EmployeeViewModel>>(employeeModel);
            if (employees.Count() == 0)
            {
                return NotFound("No Employees Found");
            }
            return Ok(employees);
        }

        [HttpGet("{id}")]
        public IActionResult GetEmployee(int id)
        {
            Employee employeeModel = _employeeService.GetById(id);
            EmployeeViewModel employee = _mapper.Map<EmployeeViewModel>(employeeModel);
            if(employee == null)
            {
                return NotFound("No Employee Found");
            }
            return Ok(employee);
        }

        [HttpPost]
        public IActionResult AddEmployee([FromBody]EmployeeViewModel employee)
        {
            Employee employeeModel = _mapper.Map<Employee>(employee);
            _employeeService.Add(employeeModel);
            return Ok(employeeModel);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateEmployee([FromRoute] int id, EmployeeViewModel employee)
        {
            Employee employeeModel = _mapper.Map<Employee>(employee);
            if(id != employee.EmployeeId)
            {
                return BadRequest();
            }
            _employeeService.Update(employeeModel);
            return Ok(employeeModel);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteEmployee(int id)
        {
            Employee employeeModel = _employeeService.GetById(id);
            if(employeeModel.EmployeeId == 0)
            {
                return NotFound("No Employee Found");
            }
            _employeeService.Delete(employeeModel);
            return Ok(employeeModel);
        }

        //[Route("GetDepartmentEmployees")]
        //[HttpGet]
        //public IActionResult GetDepartmentEmployees()
        //{
        //    IEnumerable<DepartmentEmployee> employees =  _employeeService.GetDepartmentEmployees();
        //    if (employees.Count() == 0)
        //    {
        //        return NotFound("No Data Found");
        //    }
        //    return Ok(employees);
        //}

        [Route("GetDepartmentEmployeesPagination")]
        [HttpGet]
        public IActionResult GetDepartmentEmployeesPagination(int pageSize, int pageNumber)
        {
            IEnumerable<DepartmentEmployee> departmentEmployees = _employeeService.GetEmployeesPagination(pageSize, pageNumber);
            if (departmentEmployees.Count() == 0)
            {
                return NotFound("No Employees Found");
            }
            EmployeesListViewModel deptEmpList = new EmployeesListViewModel();
            deptEmpList.DepartmentEmployeeList = departmentEmployees;
            var allList = _employeeService.GetDepartmentEmployees();
            deptEmpList.TotalCount = allList.Count();
            //deptEmpList.TotalCount = _employeeService.GetDepartmentEmployees().Count();
            return Ok(deptEmpList);
        }

        //[HttpGet]
        //[Route("GetPaginationEmployees")]
        //public IActionResult GetPaginationEmployees(int pageSize, int pageNumber)
        //{
        //    IEnumerable<Employee> employeeModel = _employeeService.GetAllPagination(pageSize, pageNumber);
        //    IEnumerable<EmployeeViewModel> employees = _mapper.Map<List<EmployeeViewModel>>(employeeModel);
        //    if (employees.Count() == 0)
        //    {
        //        return NotFound("No Employees Found");
        //    }
        //    EmployeesListViewModel empList = new EmployeesListViewModel();
        //    empList.EmployeeList = employees;
        //    empList.TotalCount = _employeeService.GetAll().Count();
        //    return Ok(empList);
        //}

    }
}