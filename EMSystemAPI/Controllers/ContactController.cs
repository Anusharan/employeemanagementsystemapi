﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EMS.Data.Models;
using EMS.Repository.Interface;
using EMS.Service.Contact;
using EMSystemAPI.ViewModel;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace EMSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : Controller
    {
        private readonly IContactService _contactService;
        private readonly IMapper _mapper;

        public ContactController(IContactService contactService, IMapper mapper)
        {
            _contactService = contactService;
            _mapper = mapper;
        }

        [HttpGet]
        [EnableCors("AllowOrigin")]
        public IActionResult GetContacts()
        {
            IEnumerable<Contact> contactModel = _contactService.GetAll();
            IEnumerable<ContactViewModel> contacts = _mapper.Map<List<ContactViewModel>>(contactModel);
            if (contacts.Count() == 0)
            {
                return NotFound("No Contact List Found");
            }
            return Ok(contacts);
        }

        [HttpGet("{id}")]
        [EnableCors("AllowOrigin")]
        public IActionResult GetContactById(int id)
        {
            Contact contactModel = _contactService.GetById(id);
            ContactViewModel contact = _mapper.Map<ContactViewModel>(contactModel);
            if (contact == null)
            {
                return NotFound("No Contact Found");
            }
            return Ok(contact);
        }

        [HttpPost]
        [EnableCors("AllowOrigin")]
        public IActionResult AddContact([FromBody]ContactViewModel model)
        {
            Contact contact = _mapper.Map<Contact>(model);
            _contactService.Add(contact);
            return Ok(contact);
        }

        [HttpPut("{id}")]
        [EnableCors("AllowOrigin")]
        public IActionResult UpdateContact([FromRoute] int id, ContactViewModel model)
        {
            Contact contact = _mapper.Map<Contact>(model);
            if (id != contact.ContactId)
            {
                return BadRequest();
            }
            _contactService.Update(contact);
            return Ok(contact);
        }

        [HttpDelete("{id}")]
        [EnableCors("AllowOrigin")]
        public IActionResult DeleteContact(int id)
        {
            Contact contact = _contactService.GetById(id);
            if(contact.ContactId == 0)
            {
                return NotFound("No Contact Found");
            }
            _contactService.Delete(contact);
            return Ok(contact);
        }
    }
}