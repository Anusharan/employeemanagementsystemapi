﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EMS.Data.Models;
using EMS.Service.Department;
using EMSystemAPI.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace EMSystemAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : Controller
    {
        private readonly IDepartmentService _departmentService;
        private readonly IMapper _mapper;

        public DepartmentController(IDepartmentService departmentService, IMapper mapper)
        {
            _departmentService = departmentService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetDepartments()
        {
            IEnumerable<Department> departmentModel = _departmentService.GetAll();
            IEnumerable<DepartmentViewModel> departments = _mapper.Map<List<DepartmentViewModel>>(departmentModel);
            if (departments.Count() == 0)
            {
                return NotFound("No Department List Found");
            }
            return Ok(departments);
        }

        [HttpGet("{id}")]
        public IActionResult GetDepartmentById(int id)
        {
            Department departmentModel = _departmentService.GetById(id);
            DepartmentViewModel department = _mapper.Map<DepartmentViewModel>(departmentModel);
            if (department == null)
            {
                return NotFound("No Department Found");
            }
            return Ok(department);
        }

        [HttpPost]
        public IActionResult AddDepartment([FromBody]DepartmentViewModel model)
        {
            Department department = _mapper.Map<Department>(model);
            _departmentService.Add(department);
            return Ok(department);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateDepartment([FromRoute] int id, DepartmentViewModel model)
        {
            Department department = _mapper.Map<Department>(model);
            if (id != department.DepartmentId)
            {
                return BadRequest();
            }
            _departmentService.Update(department);
            return Ok(department);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteDepartment(int id)
        {
            Department department = _departmentService.GetById(id);
            if (department.DepartmentId == 0)
            {
                return NotFound("No Department Found");
            }
            _departmentService.Delete(department);
            return Ok(department);
        }
    }
}