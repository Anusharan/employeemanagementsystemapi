﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMS.Repository.Interface
{
    public interface IRepository<T> where T: class
    {
        IQueryable<T> Table { get; }

        IQueryable<T> TableNoTracking { get; }

        IEnumerable<T> GetAllPagination(int pageSize, int pageNumber);

        IEnumerable<T> GetAll();

        T GetById(int id);

        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);

    }
}
