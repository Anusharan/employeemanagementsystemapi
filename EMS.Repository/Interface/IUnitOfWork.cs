﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Repository.Interface
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
    }
}
