﻿using EMS.Data.Models;
using EMS.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly EmployeeManagementSystemContext _DbContext;

        public UnitOfWork(EmployeeManagementSystemContext Context)
        {
            this._DbContext = Context;
        }
        public void Commit()
        {
            this._DbContext.SaveChanges();
        }

        public void Dispose()
        {
            this._DbContext.Dispose();
        }
    }
}
