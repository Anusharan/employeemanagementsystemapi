﻿using Dapper;
using EMS.Data.Models;
using EMS.Data.Models.ViewModel;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Repository.DepartmentRepo
{
    public class EmployeeRepository: IEmployeeRepository
    {
        private readonly IConfiguration _config;

        public EmployeeRepository(IConfiguration config)
        {
            _config = config;
        }

        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_config.GetConnectionString("EMSDBConnection"));
            }
        }

        public IEnumerable<DepartmentEmployee> GetDepartmentEmployee()
        {
            using (IDbConnection conn = Connection)
            {
                string sQuery = "Select Employee.EmployeeID, Employee.ContactID, Employee.CompanyID, Employee.UserID, Employee.DepartmentID, Employee.StartDate, Employee.EndDate, Employee.Salary, Employee.IsAdim, Employee.IsActive, Department.DepartmentName, Department.DepartmentDescription, Contact.FirstName, Contact.LastName, Contact.Email, Contact.PhoneNumber, Contact.Address, Contact.Country, Contact.City, Contact.Gender From Employee Left Outer Join Department ON Employee.DepartmentID = Department.DepartmentID INNER JOIN Contact ON Employee.ContactID = Contact.ContactID ";
                conn.Open();
                var result = conn.Query<DepartmentEmployee>(sQuery);
                return result;
            }
        }

        public IEnumerable<DepartmentEmployee> GetEmployeesPagination(int pageSize, int pageNumber)
        {
            using (IDbConnection conn = Connection)
            {
                int ExcludeRecords = (pageSize * pageNumber) - pageSize;
                StringBuilder strQuery = new StringBuilder();
                strQuery.AppendFormat(@"Select Employee.EmployeeID, Employee.ContactID, Employee.CompanyID, Employee.UserID, Employee.DepartmentID, Employee.StartDate, Employee.EndDate, Employee.Salary, Employee.IsAdim, Employee.IsActive, Department.DepartmentName, Department.DepartmentDescription, Contact.FirstName, Contact.LastName, Contact.Email, Contact.PhoneNumber, Contact.Address, Contact.Country, Contact.City, Contact.Gender From Employee Left Join Department ON Employee.DepartmentID = Department.DepartmentID INNER JOIN Contact ON Employee.ContactID = Contact.ContactID ORDER BY  Employee.CreatedTS desc OFFSET @ExcludeRecords ROWS FETCH NEXT @pageSize ROWS ONLY ");
                var parameters = new DynamicParameters();
                parameters.Add("ExcludeRecords", ExcludeRecords);
                parameters.Add("pageSize", pageSize);
                conn.Open();
                var result = conn.Query<DepartmentEmployee>(strQuery.ToString(), parameters);
                return result;
            }
        }
    }
}
