﻿using EMS.Data.Models;
using EMS.Data.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EMS.Repository.DepartmentRepo
{
    public interface IEmployeeRepository
    {
        IEnumerable<DepartmentEmployee> GetDepartmentEmployee();
        IEnumerable<DepartmentEmployee> GetEmployeesPagination(int pageSize, int pageNumber);


    }
}
