﻿using EMS.Data.Models;
using EMS.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMS.Repository
{
    public class GenericRepository<T> : IRepository<T> where T:class
    {
        private readonly EmployeeManagementSystemContext _DbContext;
        private readonly DbSet<T> _DbSet;

        public GenericRepository(EmployeeManagementSystemContext context)
        {
            this._DbContext = context;
            this._DbSet = this._DbContext.Set<T>();
        }

        public IQueryable<T> Table => this._DbSet.AsQueryable();
        public IQueryable<T> TableNoTracking => this._DbSet.AsNoTracking().AsQueryable();


        public void Add(T entity)
        {
            this._DbSet.Add(entity);
        }

        public void Delete(T entity)
        {
            this._DbSet.Remove(entity);
        }

        public IEnumerable<T> GetAllPagination(int pageSize, int pageNumber)
        {
            int ExcludeRecords = (pageSize * pageNumber) - pageSize;
            return this._DbSet.Skip(ExcludeRecords).Take(pageSize);
        }

        public IEnumerable<T> GetAll()
        {
            return this._DbSet.ToList();
        }

        public T GetById(int id)
        {
            return this._DbSet.Find(id);
        }

        public void Update(T entity)
        {
            this._DbSet.Attach(entity);
            this._DbContext.Entry(entity).State = EntityState.Modified;
        }
    }
}


