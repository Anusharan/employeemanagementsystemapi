﻿using System;
using System.Collections.Generic;

namespace EMS.Data.Models
{
    public partial class Employee
    {
        public Employee()
        {
            //EmployeeProjectRelationship = new HashSet<EmployeeProjectRelationship>();
            //EmployeeSchedule = new HashSet<EmployeeSchedule>();
        }

        public int EmployeeId { get; set; }
        public int ContactId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? Salary { get; set; }
        public string CurrentAddress { get; set; }
        public string CurrentCity { get; set; }
        public string CurrentCountry { get; set; }
        public bool? IsAdim { get; set; }
        public DateTime? CreatedTs { get; set; }
        public DateTime? ModifiedTs { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public bool? IsActive { get; set; }
        public int? DepartmentId { get; set; }
        public int? CompanyId { get; set; }
        public string UserId { get; set; }

        public virtual Company Company { get; set; }
        public virtual Contact Contact { get; set; }
        public virtual Department Department { get; set; }
        //public virtual User User { get; set; }
        //public virtual ICollection<EmployeeProjectRelationship> EmployeeProjectRelationship { get; set; }
        //public virtual ICollection<EmployeeSchedule> EmployeeSchedule { get; set; }
    }
}
