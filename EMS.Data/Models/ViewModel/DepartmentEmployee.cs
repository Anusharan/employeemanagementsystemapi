﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.Data.Models.ViewModel
{
    public class DepartmentEmployee
    {
        public int EmployeeId { get; set; }
        public int ContactId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? Salary { get; set; }
        public bool? IsAdim { get; set; }
        public DateTime? CreatedTs { get; set; }
        public DateTime? ModifiedTs { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public bool? IsActive { get; set; }
        public int? DepartmentId { get; set; }
        public int? CompanyId { get; set; }
        public string UserId { get; set; }

        public string DepartmentName { get; set; }
        public string DepartmentDescription { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Gender { get; set; }

        public DepartmentEmployee()
        {
            CreatedTs = DateTime.Now;
            ModifiedTs = DateTime.Now;
            CreatedBy = 1;
            CompanyId = 1;
        }
    }
}
