﻿using System;
using System.Collections.Generic;

namespace EMS.Data.Models
{
    public partial class Contact
    {
        public Contact()
        {
            Employee = new HashSet<Employee>();
        }

        public int ContactId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public DateTime? CreatedTs { get; set; }
        public DateTime? ModifiedTs { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public int? CompanyId { get; set; }
        public string Gender { get; set; }
        public string Image { get; set; }


        public virtual Company Company { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
    }
}
