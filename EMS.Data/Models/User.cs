﻿//using System;
//using System.Collections.Generic;

//namespace EMS.Data.Models
//{
//    public partial class User
//    {
//        public User()
//        {
//            Employee = new HashSet<Employee>();
//            UserRoleRelationship = new HashSet<UserRoleRelationship>();
//        }

//        public string UserId { get; set; }
//        public string FirstName { get; set; }
//        public string LastName { get; set; }
//        public int? CompanyId { get; set; }
//        public string Password { get; set; }
//        public string Email { get; set; }
//        public string Address { get; set; }
//        public string Country { get; set; }
//        public string City { get; set; }
//        public bool? IsActive { get; set; }
//        public int? UserRoleId { get; set; }
//        public DateTime? CreatedTs { get; set; }
//        public DateTime? ModifiedTs { get; set; }
//        public int? CreatedBy { get; set; }
//        public int? ModifiedBy { get; set; }
//        public string Title { get; set; }

//        public Company Company { get; set; }
//        public ICollection<Employee> Employee { get; set; }
//        public ICollection<UserRoleRelationship> UserRoleRelationship { get; set; }
//    }
//}
