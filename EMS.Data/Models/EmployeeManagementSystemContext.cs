﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace EMS.Data.Models
{
    public partial class EmployeeManagementSystemContext : DbContext
    {
        public EmployeeManagementSystemContext()
        {
        }

        public EmployeeManagementSystemContext(DbContextOptions<EmployeeManagementSystemContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<Contact> Contact { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        //public virtual DbSet<EmployeeProjectRelationship> EmployeeProjectRelationship { get; set; }
        //public virtual DbSet<EmployeeSchedule> EmployeeSchedule { get; set; }
        //public virtual DbSet<Leave> Leave { get; set; }
        //public virtual DbSet<LeaveLog> LeaveLog { get; set; }
        //public virtual DbSet<Project> Project { get; set; }
        //public virtual DbSet<User> User { get; set; }
        //public virtual DbSet<UserRole> UserRole { get; set; }
        //public virtual DbSet<UserRoleRelationship> UserRoleRelationship { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Server=localhost\\MSSQLSERVER01;Database=EmployeeManagementSystem;UID=DESKTOP-C1J48OS\\anush;Trusted_Connection=True;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>(entity =>
            {
                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.Address).HasMaxLength(50);

                entity.Property(e => e.City)
                    .HasColumnName("CIty")
                    .HasMaxLength(50);

                entity.Property(e => e.CompanyName).HasMaxLength(50);

                entity.Property(e => e.Country).HasMaxLength(50);

                entity.Property(e => e.CreatedTs)
                    .HasColumnName("CreatedTS")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmailAddress).HasMaxLength(50);

                entity.Property(e => e.ModifiedTs)
                    .HasColumnName("ModifiedTS")
                    .HasColumnType("datetime");

                entity.Property(e => e.PhoneNumber).HasMaxLength(25);

                entity.Property(e => e.State).HasMaxLength(50);

                entity.Property(e => e.Website).HasMaxLength(50);
            });

            modelBuilder.Entity<Contact>(entity =>
            {
                entity.Property(e => e.ContactId).HasColumnName("ContactID");

                entity.Property(e => e.Address).HasMaxLength(50);

                entity.Property(e => e.City)
                    .HasColumnName("CIty")
                    .HasMaxLength(50);

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.Country).HasMaxLength(50);

                entity.Property(e => e.CreatedTs)
                    .HasColumnName("CreatedTS")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.FirstName).HasMaxLength(50);

                entity.Property(e => e.LastName).HasMaxLength(50);

                entity.Property(e => e.ModifiedTs)
                    .HasColumnName("ModifiedTS")
                    .HasColumnType("datetime");

                entity.Property(e => e.PhoneNumber).HasMaxLength(50);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Contact)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_ContactCompanyID");

                entity.Property(e => e.Gender).HasMaxLength(15);

            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.DepartmentDescription).HasMaxLength(250);

                entity.Property(e => e.DepartmentName).HasMaxLength(50);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Department)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_DepartmentCompany");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.ContactId).HasColumnName("ContactID");

                entity.Property(e => e.CreatedTs)
                    .HasColumnName("CreatedTS")
                    .HasColumnType("datetime");

                entity.Property(e => e.CurrentAddress).HasMaxLength(50);

                entity.Property(e => e.CurrentCity).HasMaxLength(50);

                entity.Property(e => e.CurrentCountry).HasMaxLength(50);

                entity.Property(e => e.DepartmentId).HasColumnName("DepartmentID");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedTs)
                    .HasColumnName("ModifiedTS")
                    .HasColumnType("datetime");

                entity.Property(e => e.Salary).HasColumnType("money");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(50);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Employee)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_EmployeeCompanyID");

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.Employee)
                    .HasForeignKey(d => d.ContactId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EmployeeContactID");

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.Employee)
                    .HasForeignKey(d => d.DepartmentId)
                    .HasConstraintName("FK_EmployeeDepartmentID");

                //entity.HasOne(d => d.User)
                //    .WithMany(p => p.Employee)
                //    .HasForeignKey(d => d.UserId)
                //    .HasConstraintName("FK_EmployeeUser");
            });

            //modelBuilder.Entity<EmployeeProjectRelationship>(entity =>
            //{
            //    entity.Property(e => e.EmployeeProjectRelationshipId).HasColumnName("EmployeeProjectRelationshipID");

            //    entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

            //    entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

            //    entity.HasOne(d => d.Employee)
            //        .WithMany(p => p.EmployeeProjectRelationship)
            //        .HasForeignKey(d => d.EmployeeId)
            //        .HasConstraintName("FK_EmployeeProjectRelationshipEmployeeID");

            //    entity.HasOne(d => d.Project)
            //        .WithMany(p => p.EmployeeProjectRelationship)
            //        .HasForeignKey(d => d.ProjectId)
            //        .HasConstraintName("FK_EmployeeProjectRelationshipProjectID");
            //});

            //modelBuilder.Entity<EmployeeSchedule>(entity =>
            //{
            //    entity.Property(e => e.EmployeeScheduleId).HasColumnName("EmployeeScheduleID");

            //    entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

            //    entity.Property(e => e.WorkDate).HasColumnType("datetime");

            //    entity.Property(e => e.WorkEndHour).HasColumnType("datetime");

            //    entity.Property(e => e.WorkStartHour).HasColumnType("datetime");

            //    entity.HasOne(d => d.Employee)
            //        .WithMany(p => p.EmployeeSchedule)
            //        .HasForeignKey(d => d.EmployeeId)
            //        .HasConstraintName("FK_EmployeeScheduleEmployeeID");
            //});

            //modelBuilder.Entity<Leave>(entity =>
            //{
            //    entity.Property(e => e.LeaveId).HasColumnName("LeaveID");

            //    entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

            //    entity.Property(e => e.LeaveDescription).HasMaxLength(50);

            //    entity.Property(e => e.LeaveStatus).HasMaxLength(50);

            //    entity.Property(e => e.LeaveType).HasMaxLength(50);

            //    entity.HasOne(d => d.Company)
            //        .WithMany(p => p.Leave)
            //        .HasForeignKey(d => d.CompanyId)
            //        .HasConstraintName("FK_LeaveCompany");
            //});

            //modelBuilder.Entity<LeaveLog>(entity =>
            //{
            //    entity.Property(e => e.LeaveLogId).HasColumnName("LeaveLogID");

            //    entity.Property(e => e.LeaveId).HasColumnName("LeaveID");

            //    entity.Property(e => e.LeaveLogDate).HasColumnType("datetime");

            //    entity.Property(e => e.LeaveLogDescription).HasMaxLength(250);

            //    entity.Property(e => e.LeaveLogName)
            //        .HasMaxLength(50)
            //        .IsUnicode(false);

            //    entity.Property(e => e.LeaveLogType).HasMaxLength(50);

            //    entity.HasOne(d => d.Leave)
            //        .WithMany(p => p.LeaveLog)
            //        .HasForeignKey(d => d.LeaveId)
            //        .HasConstraintName("FK_LeaveLog_LeaveID");
            //});

            //modelBuilder.Entity<Project>(entity =>
            //{
            //    entity.Property(e => e.ProjectId).HasColumnName("ProjectID");

            //    entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

            //    entity.Property(e => e.ProjectDescription).HasMaxLength(250);

            //    entity.Property(e => e.ProjectName).HasMaxLength(50);

            //    entity.HasOne(d => d.Company)
            //        .WithMany(p => p.Project)
            //        .HasForeignKey(d => d.CompanyId)
            //        .HasConstraintName("FK_ProjectCompanyID");
            //});

            //modelBuilder.Entity<User>(entity =>
            //{
            //    entity.Property(e => e.UserId)
            //        .HasColumnName("UserID")
            //        .HasMaxLength(50)
            //        .ValueGeneratedNever();

            //    entity.Property(e => e.Address).HasMaxLength(100);

            //    entity.Property(e => e.City).HasMaxLength(50);

            //    entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

            //    entity.Property(e => e.Country).HasMaxLength(50);

            //    entity.Property(e => e.CreatedTs)
            //        .HasColumnName("CreatedTS")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.Email).HasMaxLength(50);

            //    entity.Property(e => e.FirstName).HasMaxLength(45);

            //    entity.Property(e => e.LastName).HasMaxLength(40);

            //    entity.Property(e => e.ModifiedTs)
            //        .HasColumnName("ModifiedTS")
            //        .HasColumnType("datetime");

            //    entity.Property(e => e.Password).HasMaxLength(50);

            //    entity.Property(e => e.Title).HasMaxLength(50);

            //    entity.Property(e => e.UserRoleId).HasColumnName("UserRoleID");

            //    entity.HasOne(d => d.Company)
            //        .WithMany(p => p.User)
            //        .HasForeignKey(d => d.CompanyId)
            //        .OnDelete(DeleteBehavior.Cascade)
            //        .HasConstraintName("FK_CompanyID");
            //});

            //modelBuilder.Entity<UserRole>(entity =>
            //{
            //    entity.Property(e => e.UserRoleId).HasColumnName("UserRoleID");

            //    entity.Property(e => e.UserRoleDescription).HasMaxLength(250);

            //    entity.Property(e => e.UserRoleName).HasMaxLength(50);
            //});

            //modelBuilder.Entity<UserRoleRelationship>(entity =>
            //{
            //    entity.Property(e => e.UserRoleRelationshipId).HasColumnName("UserRoleRelationshipID");

            //    entity.Property(e => e.UserId)
            //        .HasColumnName("UserID")
            //        .HasMaxLength(50);

            //    entity.Property(e => e.UserRoleId).HasColumnName("UserRoleID");

            //    entity.HasOne(d => d.User)
            //        .WithMany(p => p.UserRoleRelationship)
            //        .HasForeignKey(d => d.UserId)
            //        .OnDelete(DeleteBehavior.Cascade)
            //        .HasConstraintName("FK_UserID");

            //    entity.HasOne(d => d.UserRole)
            //        .WithMany(p => p.UserRoleRelationship)
            //        .HasForeignKey(d => d.UserRoleId)
            //        .OnDelete(DeleteBehavior.Cascade)
            //        .HasConstraintName("FK_UserRoleID");
            //});
        }
    }
}
