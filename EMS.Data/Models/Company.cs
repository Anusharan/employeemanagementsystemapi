﻿using System;
using System.Collections.Generic;

namespace EMS.Data.Models
{
    public partial class Company
    {
        public Company()
        {
            Contact = new HashSet<Contact>();
            Department = new HashSet<Department>();
            Employee = new HashSet<Employee>();
            //Leave = new HashSet<Leave>();
            //Project = new HashSet<Project>();
            //User = new HashSet<User>();
        }

        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Website { get; set; }
        public DateTime? CreatedTs { get; set; }
        public DateTime? ModifiedTs { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }

        public virtual ICollection<Contact> Contact { get; set; }
        public virtual ICollection<Department> Department { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        //public ICollection<Leave> Leave { get; set; }
        //public ICollection<Project> Project { get; set; }
        //public ICollection<User> User { get; set; }
    }
}
