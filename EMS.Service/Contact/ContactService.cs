﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EMS.Data.Models;
using EMS.Repository.Interface;
using ContactModel = EMS.Data.Models.Contact;
using EmployeeModel = EMS.Data.Models.Employee;



namespace EMS.Service.Contact
{
    public class ContactService : IContactService
    {
        private readonly IRepository<ContactModel> _contactRepository;
        private readonly IRepository<EmployeeModel> _employeeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ContactService(IRepository<ContactModel> contactRepository, IRepository<EmployeeModel> employeeRepository, IUnitOfWork unitOfWork)
        {
            _contactRepository = contactRepository;
            _employeeRepository = employeeRepository;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<ContactModel> GetAll()
        {
            return _contactRepository.GetAll();
        }

        public IEnumerable<ContactModel> GetAllPagination(int pageSize, int pageNumber)
        {
            throw new NotImplementedException();
        }

        public ContactModel GetById(int id)
        {
            return _contactRepository.GetById(id);
        }

        public void Add(ContactModel entity)
        {
            _contactRepository.Add(entity);
            _unitOfWork.Commit();
        }

        public void Update(ContactModel entity)
        {
            _contactRepository.Update(entity);
            _unitOfWork.Commit();
        }

        public void Delete(ContactModel entity)
        {
            EmployeeModel employee = _employeeRepository.Table.FirstOrDefault(x => x.ContactId == entity.ContactId);
            if(employee != null)
            {
                _employeeRepository.Delete(employee);
            }
            _contactRepository.Delete(entity);
            _unitOfWork.Commit();
        }

        public IEnumerable<ContactModel> GetAll(int pageSize, int pageNumber)
        {
            throw new NotImplementedException();
        }
    }
}
