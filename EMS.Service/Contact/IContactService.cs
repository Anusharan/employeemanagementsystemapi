﻿using System;
using System.Collections.Generic;
using System.Text;
using EMS.Data.Models;
using ContactModel = EMS.Data.Models.Contact;

namespace EMS.Service.Contact
{
    public interface IContactService : IBaseService<ContactModel>
    {
    }
}
