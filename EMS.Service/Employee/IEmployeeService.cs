﻿using EMS.Data.Models.ViewModel;
using EMS.Repository.DepartmentRepo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EmployeeModel = EMS.Data.Models.Employee;

namespace EMS.Service.Employee
{
    public interface IEmployeeService : IBaseService<EmployeeModel>
    {
        IEnumerable<DepartmentEmployee> GetDepartmentEmployees();
        IEnumerable<DepartmentEmployee> GetEmployeesPagination(int pageSize, int pageNumber);

    }
}
