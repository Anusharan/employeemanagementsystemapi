﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EMS.Data.Models;
using EMS.Data.Models.ViewModel;
using EMS.Repository.DepartmentRepo;
using EMS.Repository.Interface;
using EmployeeModel = EMS.Data.Models.Employee;


namespace EMS.Service.Employee
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IRepository<EmployeeModel> _employeeRepository;
        private readonly IEmployeeRepository _selfEmployeeRepository;
        private readonly IUnitOfWork _unitOfWork;

        public EmployeeService(IRepository<EmployeeModel> employeeRepository, IEmployeeRepository selfEmployeeRepository, IUnitOfWork unitOfWork)
        {
            _employeeRepository = employeeRepository;
            _selfEmployeeRepository = selfEmployeeRepository;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<EmployeeModel> GetAll()
        {
            return _employeeRepository.GetAll();
        }

        //public IEnumerable<EmployeeModel> GetAllPagination(int pageSize, int pageNumber)
        //{
        //    return _employeeRepository.GetAllPagination(pageSize, pageNumber);
        //}

        public EmployeeModel GetById(int id)
        {
            return _employeeRepository.GetById(id);
        }

        public void Add(EmployeeModel entity)
        {
            List<EmployeeModel> existingEmployees = _employeeRepository.GetAll().ToList();
            if (existingEmployees.Any(x => x.ContactId == entity.ContactId && x.EmployeeId != entity.EmployeeId))
            {
                throw new Exception("Employee with same contact Id already exists");
            }
            if (existingEmployees.Any(x => x.EmployeeId == entity.EmployeeId && x.DepartmentId == entity.DepartmentId))
            {
                throw new Exception("Employee cannot belong to multiple departments.");
            }
            _employeeRepository.Add(entity);
            _unitOfWork.Commit();
        }

        public void Update(EmployeeModel entity)
        {
            List<EmployeeModel> existingEmployees = _employeeRepository.TableNoTracking.ToList();
            if (existingEmployees.Any(x => x.ContactId == entity.ContactId && x.EmployeeId != entity.EmployeeId))
            {
                throw new Exception("Contact Id already exists");
            }
            _employeeRepository.Update(entity);
            _unitOfWork.Commit();
        }

        public void Delete(EmployeeModel entity)
        {
            _employeeRepository.Delete(entity);
            _unitOfWork.Commit();
        }

        public IEnumerable<DepartmentEmployee> GetDepartmentEmployees()
        {
            return _selfEmployeeRepository.GetDepartmentEmployee();
        }

        public IEnumerable<DepartmentEmployee> GetEmployeesPagination(int pageSize, int pageNumber)
        {
            return _selfEmployeeRepository.GetEmployeesPagination(pageSize, pageNumber);

        }
    }
}
