﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.Service
{
    public interface IBaseService<T> where T: class
    {
        T GetById(int id);

        IEnumerable<T> GetAll();

        //IEnumerable<T> GetAllPagination(int pageSize, int pageNumber);

        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);
    }
}
