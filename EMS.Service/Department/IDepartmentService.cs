﻿using System;
using System.Collections.Generic;
using System.Text;
using DepartmentModel = EMS.Data.Models.Department;


namespace EMS.Service.Department
{
    public interface IDepartmentService : IBaseService<DepartmentModel>
    {
    }
}
