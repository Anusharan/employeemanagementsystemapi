﻿using System;
using System.Collections.Generic;
using System.Text;
using EMS.Data.Models;
using EMS.Repository.Interface;
using DepartmentModel = EMS.Data.Models.Department;

namespace EMS.Service.Department
{
    public class DepartmentService : IDepartmentService
    {
        private readonly IRepository<DepartmentModel> _departmentRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DepartmentService(IRepository<DepartmentModel> departmentRepository, IUnitOfWork unitOfWork)
        {
            _departmentRepository = departmentRepository;
            _unitOfWork = unitOfWork;
        }

        public DepartmentModel GetById(int id)
        {
            return _departmentRepository.GetById(id);
        }

        public IEnumerable<DepartmentModel> GetAll()
        {
            return _departmentRepository.GetAll();
        }

        public IEnumerable<DepartmentModel> GetAllPagination(int pageSize, int pageNumber)
        {
            throw new NotImplementedException();
        }

        public void Add(DepartmentModel entity)
        {
            _departmentRepository.Add(entity);
            _unitOfWork.Commit();
        }

        public void Delete(DepartmentModel entity)
        {
            _departmentRepository.Delete(entity);
            _unitOfWork.Commit();
        }

        public void Update(DepartmentModel entity)
        {
            _departmentRepository.Update(entity);
            _unitOfWork.Commit();
        }
    }
}
